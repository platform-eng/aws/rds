output "security_group_name" {
  value = aws_security_group.this.0.name
}

output "security_group_id" {
  value = aws_security_group.this.0.id
}
