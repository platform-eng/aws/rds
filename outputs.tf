output "rds_details" {
  value     = module.rds
  sensitive = true
}

output "ssm_details" {
  value     = aws_ssm_parameter.rds_master_user[*]
  sensitive = true
}
