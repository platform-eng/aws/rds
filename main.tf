module "vpc_details" {
  source       = "git::https://gitlab.com/terraswifft/platform/starter/vpc_details.git?ref=main"
  for_each     = var.rds_conf
  vpc_id       = try(each.value.vpc_id, var.vpc_id)
  vpc_name     = try(each.value.vpc_name, var.vpc_name)
  subnet_ids   = try(each.value.subnet_ids, var.subnet_ids)
  subnet_names = try(each.value.subnet_names, var.subnet_names)
}

module "rds" {
  source   = "terraform-aws-modules/rds/aws"
  version  = "5.9.0"
  for_each = var.rds_conf
  # insert the 1 required variable here
  identifier                             = each.key
  instance_use_identifier_prefix         = try(each.value.instance_use_identifier_prefix, var.instance_use_identifier_prefix)
  custom_iam_instance_profile            = try(each.value.custom_iam_instance_profile, var.custom_iam_instance_profile)
  allocated_storage                      = try(each.value.allocated_storage, var.allocated_storage)
  storage_type                           = try(each.value.storage_type, var.storage_type)
  storage_throughput                     = try(each.value.storage_throughput, var.storage_throughput)
  storage_encrypted                      = try(each.value.storage_encrypted, var.storage_encrypted)
  kms_key_id                             = try(each.value.kms_key_id, var.kms_key_id)
  replicate_source_db                    = try(each.value.replicate_source_db, var.replicate_source_db)
  license_model                          = try(each.value.license_model, var.license_model)
  replica_mode                           = try(each.value.replica_mode, var.replica_mode)
  iam_database_authentication_enabled    = try(each.value.iam_database_authentication_enabled, var.iam_database_authentication_enabled)
  domain                                 = try(each.value.domain, var.domain)
  domain_iam_role_name                   = try(each.value.domain_iam_role_name, var.domain_iam_role_name)
  engine                                 = try(each.value.engine, var.engine)
  engine_version                         = try(each.value.engine_version, var.engine_version)
  skip_final_snapshot                    = try(each.value.skip_final_snapshot, var.skip_final_snapshot)
  snapshot_identifier                    = try(each.value.snapshot_identifier, var.snapshot_identifier)
  copy_tags_to_snapshot                  = try(each.value.copy_tags_to_snapshot, var.copy_tags_to_snapshot)
  final_snapshot_identifier_prefix       = try(each.value.final_snapshot_identifier_prefix, var.final_snapshot_identifier_prefix)
  instance_class                         = try(each.value.instance_class, var.instance_class)
  db_name                                = try(each.value.db_name, var.db_name)
  username                               = try(each.value.username, var.username)
  password                               = try(each.value.password, var.password)
  port                                   = try(each.value.port, var.port)
  vpc_security_group_ids                 = try(each.value.vpc_security_group_ids, var.vpc_security_group_ids)
  availability_zone                      = try(each.value.availability_zone, var.availability_zone)
  multi_az                               = try(each.value.multi_az, var.multi_az)
  iops                                   = try(each.value.iops, var.iops)
  publicly_accessible                    = try(each.value.publicly_accessible, var.publicly_accessible)
  monitoring_interval                    = try(each.value.monitoring_interval, var.monitoring_interval)
  monitoring_role_arn                    = try(each.value.monitoring_role_arn, var.monitoring_role_arn)
  monitoring_role_name                   = try(each.value.monitoring_role_name, var.monitoring_role_name)
  monitoring_role_use_name_prefix        = try(each.value.monitoring_role_use_name_prefix, var.monitoring_role_use_name_prefix)
  monitoring_role_description            = try(each.value.monitoring_role_description, var.monitoring_role_description)
  create_monitoring_role                 = try(each.value.create_monitoring_role, var.create_monitoring_role)
  monitoring_role_permissions_boundary   = try(each.value.monitoring_role_permissions_boundary, var.monitoring_role_permissions_boundary)
  allow_major_version_upgrade            = try(each.value.allow_major_version_upgrade, var.allow_major_version_upgrade)
  auto_minor_version_upgrade             = try(each.value.auto_minor_version_upgrade, var.auto_minor_version_upgrade)
  apply_immediately                      = try(each.value.apply_immediately, var.apply_immediately)
  maintenance_window                     = try(each.value.maintenance_window, var.maintenance_window)
  blue_green_update                      = try(each.value.blue_green_update, var.blue_green_update)
  backup_retention_period                = try(each.value.backup_retention_period, var.backup_retention_period)
  backup_window                          = try(each.value.backup_window, var.backup_window)
  restore_to_point_in_time               = try(each.value.restore_to_point_in_time, var.restore_to_point_in_time)
  s3_import                              = try(each.value.s3_import, var.s3_import)
  tags                                   = try(each.value.tags, var.tags)
  db_instance_tags                       = try(each.value.db_instance_tags, var.db_instance_tags)
  db_option_group_tags                   = try(each.value.db_option_group_tags, var.db_option_group_tags)
  db_parameter_group_tags                = try(each.value.db_parameter_group_tags, var.db_parameter_group_tags)
  db_subnet_group_tags                   = try(each.value.db_subnet_group_tags, var.db_subnet_group_tags)
  create_db_subnet_group                 = try(each.value.create_db_subnet_group, var.create_db_subnet_group)
  db_subnet_group_name                   = try(each.value.db_subnet_group_name, var.db_subnet_group_name)
  db_subnet_group_use_name_prefix        = try(each.value.db_subnet_group_use_name_prefix, var.db_subnet_group_use_name_prefix)
  db_subnet_group_description            = try(each.value.db_subnet_group_description, var.db_subnet_group_description)
  subnet_ids                             = module.vpc_details[each.key].subnet_ids
  create_db_parameter_group              = try(each.value.create_db_parameter_group, var.create_db_parameter_group)
  parameter_group_name                   = try(each.value.parameter_group_name, var.parameter_group_name)
  parameter_group_use_name_prefix        = try(each.value.parameter_group_use_name_prefix, var.parameter_group_use_name_prefix)
  parameter_group_description            = try(each.value.parameter_group_description, var.parameter_group_description)
  family                                 = try(each.value.family, var.family)
  parameters                             = try(each.value.parameters, var.parameters)
  create_db_option_group                 = try(each.value.create_db_option_group, var.create_db_option_group)
  option_group_name                      = try(each.value.option_group_name, var.option_group_name)
  option_group_use_name_prefix           = try(each.value.option_group_use_name_prefix, var.option_group_use_name_prefix)
  option_group_description               = try(each.value.option_group_description, var.option_group_description)
  major_engine_version                   = try(each.value.major_engine_version, var.major_engine_version)
  options                                = try(each.value.options, var.options)
  create_db_instance                     = try(each.value.create_db_instance, var.create_db_instance)
  timezone                               = try(each.value.timezone, var.timezone)
  character_set_name                     = try(each.value.character_set_name, var.character_set_name)
  nchar_character_set_name               = try(each.value.nchar_character_set_name, var.nchar_character_set_name)
  enabled_cloudwatch_logs_exports        = try(each.value.enabled_cloudwatch_logs_exports, var.enabled_cloudwatch_logs_exports)
  timeouts                               = try(each.value.timeouts, var.timeouts)
  option_group_timeouts                  = try(each.value.option_group_timeouts, var.option_group_timeouts)
  deletion_protection                    = try(each.value.deletion_protection, var.deletion_protection)
  performance_insights_enabled           = try(each.value.performance_insights_enabled, var.performance_insights_enabled)
  performance_insights_retention_period  = try(each.value.performance_insights_retention_period, var.performance_insights_retention_period)
  performance_insights_kms_key_id        = try(each.value.performance_insights_kms_key_id, var.performance_insights_kms_key_id)
  max_allocated_storage                  = try(each.value.max_allocated_storage, var.max_allocated_storage)
  ca_cert_identifier                     = try(each.value.ca_cert_identifier, var.ca_cert_identifier)
  delete_automated_backups               = try(each.value.delete_automated_backups, var.delete_automated_backups)
  create_random_password                 = try(each.value.create_random_password, var.create_random_password)
  random_password_length                 = try(each.value.random_password_length, var.random_password_length)
  network_type                           = try(each.value.network_type, var.network_type)
  create_cloudwatch_log_group            = try(each.value.create_cloudwatch_log_group, var.create_cloudwatch_log_group)
  cloudwatch_log_group_retention_in_days = try(each.value.cloudwatch_log_group_retention_in_days, var.cloudwatch_log_group_retention_in_days)
  cloudwatch_log_group_kms_key_id        = try(each.value.cloudwatch_log_group_kms_key_id, var.cloudwatch_log_group_kms_key_id)
}


module "security_group" {
  source                 = "./security_group"
  for_each               = var.rds_conf
  create_security_group  = try(each.value.create_security_group, var.create_security_group)
  name                   = try(each.value.create_security_group, var.create_security_group) ? try(each.value.security_group_name, var.security_group_name) : var.security_group_name
  description            = try(each.value.create_security_group, var.create_security_group) ? try(each.value.security_group_description, var.security_group_description) : var.security_group_description
  vpc_id                 = module.vpc_details[each.key].vpc_id
  revoke_rules_on_delete = try(each.value.create_security_group, var.create_security_group) ? try(each.value.security_group_revoke_rules_on_delete, var.security_group_revoke_rules_on_delete) : var.security_group_revoke_rules_on_delete
  tags                   = try(each.value.tags, var.tags)
  ingress_rules          = try(each.value.create_security_group, var.create_security_group) ? try(each.value.security_group_ingress_rules, var.security_group_ingress_rules) : var.security_group_ingress_rules
  egress_rules           = try(each.value.create_security_group, var.create_security_group) ? try(each.value.security_group_egress_rules, var.security_group_egress_rules) : var.security_group_egress_rules
}

# Store master user_name and password to `Systems Manager` -> `Parameter Store`
resource "aws_ssm_parameter" "rds_master_user" {
  for_each = module.rds
  name        = "/rds/${each.key}/MASTER_USER"
  description = " username and password for ${each.key} rds instance"
  type        = "SecureString"
  value       = "${each.value.db_instance_username},${each.value.db_instance_password}"
}