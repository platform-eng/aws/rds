variable "create_security_group" {
  type    = bool
  default = false
}

variable "name" {
  type = string

}

variable "description" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "revoke_rules_on_delete" {
  type    = bool
  default = true
}

variable "tags" {
  type    = any
  default = {}
}

variable "ingress_rules" {
  type    = list(any)
  default = []
}

variable "egress_rules" {
  type    = list(any)
  default = []
}
